import "./App.css";
import EvaluationForm from "./components/evaluation-form/evaluation-form";

function App() {
  return (
    <>
      <EvaluationForm />
    </>
  );
}

export default App;
