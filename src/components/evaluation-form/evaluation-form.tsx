import { Field, Form } from "react-final-form";
import { onSubmit } from "../utils/validation";
import StyledWrapper from "../Wrapper/Wrapper";

type Props = {};

const EvaluationForm = (props: Props) => {
  return (
    <StyledWrapper>
      <Form onSubmit={onSubmit}>
        {(props: any) => (
          <form onSubmit={props.handleSubmit}>
            <Field name="myField">
              {(props) => (
                <div>
                  <input {...props.input} id="input-evaluation" />
                </div>
              )}
            </Field>

            <button type="submit" id="btn-submit">
              Submit
            </button>
          </form>
        )}
      </Form>
    </StyledWrapper>
  );
};

export default EvaluationForm;
