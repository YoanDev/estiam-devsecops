import { render, fireEvent } from "@testing-library/react";
import EvaluationForm from "./evaluation-form";

global.console = {
  error: jest.fn(),
  info: jest.fn(),
} as unknown as Console;

describe("EvaluationForm", () => {
  it("should be defined", () => {
    const result = render(<EvaluationForm />);
    expect(result).toBeDefined();
  });
  it("should match the regexp and return valid", () => {
    const validString = "test";
    render(<EvaluationForm />);
    const input = document.querySelector("#input-evaluation");
    const btn = document.querySelector("#btn-submit");
    if (input && btn) {
      fireEvent.change(input, { target: { value: validString } });
      fireEvent.click(btn);
    } else {
    }

    expect(console.info).toHaveBeenCalledTimes(1);
  });
  it("should not match the regexp and return invalid", () => {
    const invalidString = "_ test";
    render(<EvaluationForm />);
    const input = document.querySelector("#input-evaluation");
    const btn = document.querySelector("#btn-submit");
    if (input && btn) {
      fireEvent.change(input, { target: { value: invalidString } });
      fireEvent.click(btn);
    } else {
    }

    expect(console.error).toHaveBeenCalledTimes(1);
  });
});
