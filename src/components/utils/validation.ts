export const onSubmit = (formValues: { myField: string }) => {
  const regexp = "^[a-zA-Z0-9]*$";
  if (!formValues.myField) {
    console.error("must at least contain 1 character");
    return;
  }

  const isMatch = formValues.myField.match(regexp);

  confirmValidation(isMatch);
};

export const confirmValidation = (match?: RegExpMatchArray | null) => {
  if (match) {
    console.info("valid");
    return "valid";
  } else {
    console.error("invalid");
    return "invalid";
  }
};
